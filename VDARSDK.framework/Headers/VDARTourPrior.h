//
// VDARTourPrior.h
// PixLive SDK
// Version 7.3.0-9064fd9
//
// Built on 2021-06-29 at 21:00:40
//
// https://www.vidinoti.com
//
// Copyright 2011-2015 Vidinoti SA. All rights reserved.
//
// Any distribution of this file or any part of the PixLive SDK without
// Vidinoti SA express consentment is forbidden.
//

#import <Foundation/Foundation.h>
#import <VDARSDK/VDARPrior.h>

/**
 Represent a tour prior information needed for model fetching requests.
 When a request is sent to Vidinoti AR servers to fetch multiple models,
 one can specify under which condition a model should be returned.
 Such a condition can be, in that case, a tour ID.
 Sending this prior will returns all models belonging to the given tour.
 */
@interface VDARTourPrior : VDARPrior 

/**
 The ID of the tour
 */
@property (nonatomic) int tourID;

/**
 Create and initalize a new tour prior with the given ID
 @param tourID the tour ID
 @return The newly created instance of the tour prior
 */
+(VDARTourPrior*)tourWithID:(int)tourID;

@end

