//
// VDARModalWebViewController.h
// PixLive SDK
// Version 7.3.0-9064fd9
//
// Built on 2021-06-29 at 21:00:40
//
// https://www.vidinoti.com
//
// Copyright 2011-2015 Vidinoti SA. All rights reserved.
//
// Any distribution of this file or any part of the PixLive SDK without
// Vidinoti SA express consentment is forbidden.
//

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>

/**
 A simple viewcontroller managing a web view which is used to display web pages from AR Models.
 
 This class has to be sub-classed in order to provide control button such as next, previous for a web page as well as a close button.
 
 The web view can be accessed with the webView property. By default, the view hierarchy is composed of a simple plain view and the web view is added as a child of this plain view. Both views occupy the whole screen.
 
 @since Version 2.1.1
 */
@interface VDARModalWebViewController : UIViewController<WKNavigationDelegate>

/**
 The initial URL used to load and create this web view controller
 
 @since Version 2.1.1
 */
@property (nonatomic,readonly) NSURL *initialURL;

/** 
 The web view used to display the content
 
 @since Version 2.1.1
 */
@property (nonatomic,readonly) WKWebView *webView;

/** Tells the controller whether or not a toolbar containing web view controls should be hidden or not. If this is hidden, a close button should still be displayed to allow users to close the view controller. 
 
 @since Version 2.1.1
 */
@property (nonatomic) BOOL toolbarHidden;

/**
 This loads the given URL in the web view.
 
 This method is called a single time after the creation of the view controller. The provided URL becomes the initialURL value. If the URL is a just a filename, it will be loaded with respect to provided model path.
 
 @param url The URL to load
 @param modelPath The model path at which to load the asked file. If the URL is an absolute URL, nil can be passed.
 @since Version 2.1.1
 */
-(void)loadURL:(NSURL*)url withModelPath:(NSString*)modelPath;



@end
