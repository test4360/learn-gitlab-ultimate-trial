//
// VDARLiveAnnotationViewController.h
// PixLive SDK
// Version 7.3.0-9064fd9
//
// Built on 2021-06-29 at 21:00:40
//
// https://www.vidinoti.com
//
// Copyright 2011-2015 Vidinoti SA. All rights reserved.
//
// Any distribution of this file or any part of the PixLive SDK without
// Vidinoti SA express consentment is forbidden.
//

#import <UIKit/UIKit.h>
#import <VDARSDK/VDARSDKController.h>

@class VDARContext;
@class VDARAnnotation;
@class VDARAnnotationView;

/**
 Provide a view rendering and displaying live Augmented reality rendering. It also implement the AnnotationJS framework allowing to create interactive annotation in JavaScript.
 */
@interface VDARLiveAnnotationViewController : UIViewController<VDARSDKControllerDelegate>

/**
 The view on which all the annotations and camera frames are rendered.
 */
@property (nonatomic,readonly) VDARAnnotationView* annotationView;

/**
 Tells wether or not this view controller is currently displayed and visible
 */
@property (nonatomic,readonly) BOOL displayed;

/**
 This method can be used to stop the augmented reality rendering currently running in the receiver and unload all related resources such as OpenGL context.
 
 This allows the usage of an OpenGL context and camera before this view controller has been unloaded by the OS.
 
 @since 2.2.1
 */
-(void)stopAndUnload;

/**
 This method is called when all annotations are removed from the screen and nothing more is displayed to the user.
 
 This can be overloaded and used to show back some instructions for the users that would have been hidden when didEnterContext: is called.
 
 By default, it does nothing.
 
 @since 3.0.4
 */
-(void)annotationViewDidBecomeEmpty;

/**
 This method is called when some annotations are shown on the screen, which was previously empty.
 
 This can be overloaded and used to hide instructions from the users that would have been displayed when annotationViewDidBecomeEmpty is called.
 
 By default, it does nothing.
 
 @see [VDARLiveAnnotationViewController annotationViewDidPresentAnnotations]
 
 @since 3.0.4
 */
-(void)annotationViewDidPresentAnnotations;


@end
