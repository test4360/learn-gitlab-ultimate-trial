//
// VDARPrior.h
// PixLive SDK
// Version 7.3.0-9064fd9
//
// Built on 2021-06-29 at 21:00:40
//
// https://www.vidinoti.com
//
// Copyright 2011-2015 Vidinoti SA. All rights reserved.
//
// Any distribution of this file or any part of the PixLive SDK without
// Vidinoti SA express consentment is forbidden.
//

#import <Foundation/Foundation.h>

#ifndef VDARPRIOR_H
#define VDARPRIOR_H

/**
 Represent a prior information needed for model fetching requests.
 
 When a request is sent to Vidinoti AR servers to fetch multiple models, one can specify under which condition a model should be returned. Conditions could be for exemple a tag (i.e. a category of models), or a geographical position. This class serves as a basis for those conditions.
 
 @warning This class should never been used "as-is" and should be sub-classed. See VDARLocalizationPrior or VDARTagPrior for subclasses.
 
 @see VDARLocalizationPrior
 @see VDARTagPrior
 */
@interface VDARPrior : NSObject 

/**
 Use the dictionary values to create a new valid VDARPrior instance with the correct values.
 
 @param d The dictionary to take the values from
 @return The new VDARPrior instance or nil if the dictionary was invalid.
 @since Version 1.0.1beta r131
 */
+(VDARPrior*)priorFromDictionary:(NSDictionary*)d;



@end

#endif
