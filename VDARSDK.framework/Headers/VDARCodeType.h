//
// VDARCodeType.h
// PixLive SDK
// Version 7.3.0-9064fd9
//
// Built on 2021-06-29 at 21:00:40
//
// https://www.vidinoti.com
//
// Copyright 2011-2015 Vidinoti SA. All rights reserved.
//
// Any distribution of this file or any part of the PixLive SDK without
// Vidinoti SA express consentment is forbidden.
//

#ifndef VDARSDK_VDARCodeType_h
#define VDARSDK_VDARCodeType_h


/** Types of codes the VDARSDK module is able to recognize.*/
typedef enum {
	VDAR_CODE_TYPE_NONE        =      0,  /**< no code, do not use it. */
	VDAR_CODE_TYPE_EAN2        =      (1L << 0),  /**< GS1 2-digit add-on */
	VDAR_CODE_TYPE_EAN5        =      (1L << 1),  /**< GS1 5-digit add-on */
	VDAR_CODE_TYPE_EAN8        =      (1L << 2),  /**< EAN-8 */
	VDAR_CODE_TYPE_UPCE        =      (1L << 3),  /**< UPC-E */
	VDAR_CODE_TYPE_ISBN10      =     (1L << 4),  /**< ISBN-10 (from EAN-13).*/
	VDAR_CODE_TYPE_UPCA        =     (1L << 5),  /**< UPC-A */
	VDAR_CODE_TYPE_EAN13       =     (1L << 6),  /**< EAN-13 */
	VDAR_CODE_TYPE_ISBN13      =     (1L << 8),  /**< ISBN-13 (from EAN-13).*/
	VDAR_CODE_TYPE_COMPOSITE   =     (1L << 9),  /**< EAN/UPC composite */
	VDAR_CODE_TYPE_I25         =     (1L << 10),  /**< Interleaved 2 of 5.*/
	VDAR_CODE_TYPE_CODE39      =     (1L << 11),  /**< Code 39.*/
	VDAR_CODE_TYPE_QRCODE      =     (1L << 12),  /**< QR Code.*/
} VDARCodeType;

#endif
