//
// VDARTagPrior.h
// PixLive SDK
// Version 7.3.0-9064fd9
//
// Built on 2021-06-29 at 21:00:40
//
// https://www.vidinoti.com
//
// Copyright 2011-2015 Vidinoti SA. All rights reserved.
//
// Any distribution of this file or any part of the PixLive SDK without
// Vidinoti SA express consentment is forbidden.
//

#import <Foundation/Foundation.h>
#import <VDARSDK/VDARPrior.h>

/**
 Represent a tag (category) prior information needed for model fetching requests.
 
 When a request is sent to Vidinoti AR servers to fetch multiple models, one can specify under which condition a model should be returned. Such a condition can be, in that case, a tag (a.k.a category). Sending this prior will returns all models belonging to the named category.
 */
@interface VDARTagPrior : VDARPrior 


/**
 Initalize a new tag prior with the given tag.
 @param tagName the tag name to create the tag prior with
 @return The newly created instance of the tag prior
 @since 2.0.0b2
 */
-(id)initWithTagName:(NSString*)tagName;


/**
 The name of the tag (i.e. the name of the category).
 */
@property (nonatomic,retain) NSString* tagName;

/**
 Create and initalize a new tag with the given name
 @param tagName the tag name to create the tag prior with
 @return The newly created instance of the tag prior
 @since 2.0.2
 */
+(VDARTagPrior*)tagWithName:(NSString*)tagName;

@end
