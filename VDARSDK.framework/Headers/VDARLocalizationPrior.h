//
// VDARLocalizationPrior.h
// PixLive SDK
// Version 7.3.0-9064fd9
//
// Built on 2021-06-29 at 21:00:40
//
// https://www.vidinoti.com
//
// Copyright 2011-2015 Vidinoti SA. All rights reserved.
//
// Any distribution of this file or any part of the PixLive SDK without
// Vidinoti SA express consentment is forbidden.
//

#import <Foundation/Foundation.h>

#import <VDARSDK/VDARPrior.h>

/**
 Represent a geographical position prior information needed for model fetching requests.
 
 When a request is sent to Vidinoti AR servers to fetch multiple models, one can specify under which condition a model should be returned. Such a condition can be, in that case, a geographical position. Sending this prior will returns all models which are X meters away from the given position. X can be set by using the searchDistance parameter. */
@interface VDARLocalizationPrior : VDARPrior 

/**
 Search radius for the AR Models in meters 
 */
@property (nonatomic) float searchDistance;

/**
 Latitude of the position on earth in WGS84 format, in degree.
 
 @see http://en.wikipedia.org/wiki/World_Geodetic_System for description of the reference system. (Standard)
 */
@property (nonatomic) float latitude; // WGS84 in degree

/**
 Longitude of the position on earth in WGS84 format, in degree.
 
 @see http://en.wikipedia.org/wiki/World_Geodetic_System for description of the reference system. (Standard)
 */
@property (nonatomic) float longitude; // WGS84 in degree
@end
