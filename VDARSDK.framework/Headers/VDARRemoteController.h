//
// VDARRemoteController.h
// PixLive SDK
// Version 7.3.0-9064fd9
//
// Built on 2021-06-29 at 21:00:40
//
// https://www.vidinoti.com
//
// Copyright 2011-2015 Vidinoti SA. All rights reserved.
//
// Any distribution of this file or any part of the PixLive SDK without
// Vidinoti SA express consentment is forbidden.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

/** The block which will be called after a remote action is completed
 @param result The resulting object if any. Some operation doesn't return anything therefore this parameter should not be used to check if the remote operation succeeded. For that purpose, the err parameter should be used. If a sync operation is done, an NSArray of NSString representing the contexts remote IDs synced is returned.
 @param err If an error occured, the err object represent it, otherwise this parameter is set to nil.
 */
typedef void (^RemoteOperationCompletionHandler)(id result, NSError *err);






typedef enum {
    VDAR_IMAGE_THUMBNAIL,
    VDAR_IMAGE_NORMAL,
	VDAR_IMAGE_TRACKING
} VDARModelImageType;

@class VDARRemoteController;

/**
 The remote controller delegate can be used to keep track of the remote operations progress.
 */
@protocol RemoteControllerDelegate <NSObject>

/**
 This method is called on the receiver when the remote controller progress has been updated.
 @param controller The remote controller from which the update took place
 @param prc The progress value of the remote operation (between 0 and 1).
 @param isReady Returns true when recognition is ready to use (default: false).
 @param folder The name of the folder which the context being downloaded is contained
 @warning The progress value might not be consecutive at each call. For exemple if a remote operation is added while one is in progress, the progress value might be lower than on the previous call. However a value of 0 always indicates the start of remote operations whereas a value of 1 always indicates the end of the remote operations.
 */
-(void)remoteController:(VDARRemoteController*)controller didProgress:(float)prc isReady:(bool)isReady folder:(NSString*)folder;


@end

/**
 The remote controller object is reponsible of interacting with Vidinoti servers to get Context and to push related data back (for stats etc...).
 @warning This class is not thread safe and every call has to be done from the main thread.
 */
@interface VDARRemoteController : NSObject

/**
 The delegate of this controller. The delegate can track the progress of the remote operations.
 */
#if __has_feature(objc_arc)
@property (nonatomic,weak) id<RemoteControllerDelegate> delegate;
#else
@property (nonatomic,assign) id<RemoteControllerDelegate> delegate;
#endif // __has_feature(objc_arc)


/**
 The remote server API URL
 
 @warning You can set this property only if you have requested the right in VDARSDKController and if a valid server certificate signature has been received. If the system hasn't the right to set the property, setting it will have no effect.
 
 */
@property (nonatomic,readonly) NSString* remoteAPIServer;

/**
 Wether or not to use the Vidinoti test server. Activating this feature will make all the requests to be sent to the ARManager platform located at https://armanager-test.vidinoti.com instead of the usual one.
 
 @warning You should not use this property in a production app.
 
 @since Version 2.0.0GM
 */
@property (nonatomic) BOOL useTestServer;


/**
 Get the shared instance of that Vidinoti AR Server controller
 @return The shared instance of the Vidinoti AR Server controller
 */
+ (VDARRemoteController*) sharedInstance;


/**
 Download the contexts which match the query of priors from the Vidinoti server and synchronize them with the local DB.
 
 This method runs asynchronously to download the required contexts and to sync them with the local DB. Existing contexts are first deleted and then replaced by the newer ones. Existing contexts which are not present anymore on Vidinoti servers but which have been retrieved using one of the tag present in the array of priors will be deleted.
 
 @param priors The priors query. All elements of that array should be of VDARPrior type.
 @param completionBlock The completion handler to be called at the end of the operation (async. operation). Can be nil.
 @see VDARPrior
 @since Version 1.0.1beta r131
 */
-(void)syncRemoteModelsAsynchronouslyWithPriors:(NSArray*)priors withCompletionBlock:(RemoteOperationCompletionHandler)completionBlock;

/**
 Download the context identified by its remote ID from the Vidinoti server and add it to the local phone context DB.
 
 This method runs asynchronously to download the required context and to add it to the DB.
 
 @param remoteID The remote ID of the context to download
 @param completionBlock The completion handler to be called at the end of the operation (async. operation). Can be nil.
 */
-(void)fetchRemoteModelAsynchronously:(NSString*)remoteID withCompletionBlock:(RemoteOperationCompletionHandler)completionBlock;

/**
 Downloads and stores the mapping between the given tag names and the contexts currently present on the device.
 This method must be called after a model synchronization and before calling VDARSDKController enableContextsWithTags.

 @param tagNames a list of tag names
 @param completionBlock the callback called once the operation is over. No data is returned and an error is present if
 an error happened.
 */
-(void)syncTagContexts:(NSArray*)tagNames withCompletionBlock:(RemoteOperationCompletionHandler)completionBlock;



@end
