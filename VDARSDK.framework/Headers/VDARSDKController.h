//
// VDARSDKController.h
// PixLive SDK
// Version 7.3.0-9064fd9
//
// Built on 2021-06-29 at 21:00:40
//
// https://www.vidinoti.com
//
// Copyright 2011-2015 Vidinoti SA. All rights reserved.
//
// Any distribution of this file or any part of the PixLive SDK without
// Vidinoti SA express consentment is forbidden.
//

#import <Foundation/Foundation.h>
#import <AvailabilityMacros.h>

#ifndef VDARSDKCONTROLLER_H
#define VDARSDKCONTROLLER_H

/**
 Notification generated when the context list has changed
 @since 2.0.0
 */
extern NSString* const VDARNotificationContextListChanged;

/**
 Notification generated when the AR view needs to be open so that we can render some AR content
 @since 5.1.0
 */
extern NSString* const VDARNotificationARViewRequested;

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <VDARSDK/VDARCode.h>
#import <VDARSDK/VDARSensor.h>
#import <CoreMotion/CoreMotion.h>


@class VDARContext;
@class CALayer;
@class VDARPrior;
@class VDARCode;
@class VDARLiveAnnotationViewController;
@protocol VDARImageReceiver;


#include "VDARGPSPoint.h"

/**
 Describe the different iOS device type
 @since 2.1.2
 */
typedef enum {
	DeviceHardwareGeneralPlatform_Unknown,
	DeviceHardwareGeneralPlatform_iPhone_1G,
	DeviceHardwareGeneralPlatform_iPhone_3G,
	DeviceHardwareGeneralPlatform_iPhone_3GS,
	DeviceHardwareGeneralPlatform_iPhone_4,
	DeviceHardwareGeneralPlatform_iPhone_4S,
	DeviceHardwareGeneralPlatform_iPhone_5,
	DeviceHardwareGeneralPlatform_iPhone_5C,
	DeviceHardwareGeneralPlatform_iPhone_5S,
	DeviceHardwareGeneralPlatform_iPhone_6,
	DeviceHardwareGeneralPlatform_iPhone_6_Plus,
	DeviceHardwareGeneralPlatform_iPhone_6S,
	DeviceHardwareGeneralPlatform_iPhone_6S_Plus,
	DeviceHardwareGeneralPlatform_iPhone_SE,
	DeviceHardwareGeneralPlatform_iPod_Touch_1G,
	DeviceHardwareGeneralPlatform_iPod_Touch_2G,
	DeviceHardwareGeneralPlatform_iPod_Touch_3G,
	DeviceHardwareGeneralPlatform_iPod_Touch_4G,
	DeviceHardwareGeneralPlatform_iPod_Touch_5G,
	DeviceHardwareGeneralPlatform_iPod_Touch_6G,
	DeviceHardwareGeneralPlatform_iPad,
	DeviceHardwareGeneralPlatform_iPad_2,
	DeviceHardwareGeneralPlatform_iPad_3,
	DeviceHardwareGeneralPlatform_iPad_4,
	DeviceHardwareGeneralPlatform_iPad_Mini,
	DeviceHardwareGeneralPlatform_iPad_Air,
	DeviceHardwareGeneralPlatform_iPad_Air_2,
	DeviceHardwareGeneralPlatform_iPad_Mini_2,
	DeviceHardwareGeneralPlatform_iPad_Mini_3,
	DeviceHardwareGeneralPlatform_iPad_Mini_4,
	DeviceHardwareGeneralPlatform_iPad_Pro_9_7_inch,
	DeviceHardwareGeneralPlatform_iPad_Pro_12_9_inch,
	DeviceHardwareGeneralPlatform_Simulator
} UIDevicePlatform;

/**
 Describe an object which is capable of sending images to an image receiver
 */
@protocol VDARImageSender <NSObject>

@required

/**
 Inform the receiver that the image stream should start.
 @since Version 1.0beta r85
 */
-(void)startImageStream;

/**
 Inform the receiver that the image stream should stop.
 @since Version 1.0beta r85
 */
-(void)stopImageStream;

/**
 * Set the receiver which will receive image frame.
 * @param receiver The image frame receiver
 * @since Version 2.0.11b
 */
-(void)setImageReceiver:(NSObject<VDARImageReceiver>*)receiver;

@end

/**
 Describe an object which is capable of receiving images to process them.
 @since Version 1.0beta r85
 */
@protocol VDARImageReceiver <NSObject>

@required

/**
 Advertise the receiver that a new frame is available for processing. The new frame is identified by the Image buffer rawFrame.
 @param rawFrame The new frame received as an ImageBuffer. The image buffer should be of the format YUV 4:2:0 biplanar ( kCVPixelFormatType_420YpCbCr8BiPlanarVideoRange ). Not respecting this format will result in an undefined behaviour. The size of the frame should be of 480x360 pixels.
 @param timestamp The time at which the frame has been received from the image source.
 @since Version 1.0beta r85
 */
-(void)didCaptureFrame:(CVImageBufferRef)rawFrame atTimestamp:(CFTimeInterval)timestamp;

@end

/**
 The protocol which should be implemented by delegates of the VDARSDKController which want to receive events when a sensor is updated.
 
 A sensor is defined as a trigger to enable/disable context. Therefore when a sensor is activated the linked context is usually also activated.
 
 A sensor can be an image recognition, an iBeacon or a VidiBeacon.
 
 This protocol must be implemented by delegates added to [VDARSDKController sensorDelegates].
 
 @see VDARContext
 @see VDARSDKController.sensorDelegates
 @since Version 5.2.0
 */
@protocol VDARSDKSensorDelegate <NSObject>

@optional

/**
 Called when a sensor is triggered (i.e. either it became activated or became non activated)
 @param sensor The sensor which has been triggered.
 @param context The context that has generated and created the sensor.
 @since Version 5.2.0
 */
-(void)didTriggerSensor:(VDARSensor*)sensor forContext:(VDARContext*)context;

/**
 Called when some values of the sensor have changed. Note that when this method is called the state of the sensor do not change (i.e. if it was triggered, it stays triggered).
 
 It can be used to monitor the distance value of beacons for example. 
 
 @param sensor The sensor whose parameters have changed
 @param context The context that has generated and created the sensor.
 @since Version 5.2.0
 */
-(void)didUpdateSensorValues:(VDARSensor*)sensor forContext:(VDARContext*)context;

@end

/**
 The protocol which should be implemented by delegates of the VDARSDKController which want to receive events when a context has been detected or lost (i.e. not detected anymore) (See the Context class for the different context state).
 
 This protocol must be implemented by delegates added to [VDARSDKController detectionDelegates].
 
 @see VDARContext
 @see VDARSDKController.detectionDelegates
 @since Version 1.0beta r85
 */
@protocol VDARSDKControllerDelegate <NSObject>

@optional

/**
 Called when a context has been detected. The detected context is given in the context parameter.
 @param context The context which has been detected.
 @since Version 5.0
 */
-(void)didEnterContext:(VDARContext*)context;

/**
 Called when a context has been lost (i.e. not anymore detected in the last frames processed). The lost context is given in the context parameter.
 @param context The context which has been lost.
 @since Version 5.0
 */
-(void)didExitContext:(VDARContext*)context;

/**
 Called when a code is detected.
 
 This will only be called if the code recognition is activated on the VDARSDKController by setting the [VDARSDKController enableCodesRecognition] to 1.
 
 Note that the recognition will only work if no VDARContext is currently recognized and being tracked.
 
 @warning QR Code having an URL field starting with vdarsdk:// will be automatically treated by the SDK. Therefore, you should not trigger any action on those codes. Those code can be identified with the special [VDARCode isSpecialCode] property set to YES. See VDARSDKController.enableCodesRecognition property for more information about special codes.
 
 @param codes An array of VDARCode objects corresponding to the recognized codes
 
 @since Version 1.1.0beta r230
 */
-(void)codesDetected:(NSArray<VDARCode*>*)codes;

/**
 Called when a notification has been posted while the application is opened. A notification list should be open (if not already) and refreshed.
 @since Version 5.1.0
 */
-(void)shouldDisplayNotificationList;

/**
 Called when some context or content requires the app to trigger a synchronization based on a given tag. The app should start the synchronization with the given tag(s) right away or at the end of the currently running synchronization.
 
 @param priors a list of VDARPrior object to launch the synchronization for.
 
 @since Verison 5.1.0
 */
-(void)contextDidRequireSynchronization:(NSArray<VDARPrior*>*)priors;

/**
 Called when device dispatch an event to the app.
 
 @param eventName name of the event
 @param eventParams string containing the event parameters
 
 @since Verison 5.1.0
 */
-(void)didDispatchEventInApp:(NSString*)eventName withParams:(NSString*)eventParams ;

/**
 Called when a special AJAX app specific request is generated. The delegate should answer with a valid string or nil if it is unsupported or an error occured.
 
 @param url URL of the request
 @param data data of the request (can be nil)
 
 @since Verison 5.1.9
 */
-(NSString*)appSpecificAJAXRequestGeneratedForURL:(NSURL*)url withData:(NSString*)data;

@required
/**
 This method is called when a fatal error has occured on the VDARSDKController.
 
 This usually happens when the system wasn't able to verify the license of Vidinoti AR SDK after too many launches / too many days. The AR system is thus deactivated and this method is called. The error domain, in that case, is set to @"com.vidinoti.VDARSDKController.CertificateSignatureError". Note that if this error happens, the user can restart the application with a valid Internet connection to correct that error.
 
 You should inform the user of the problem.
 
 @param err The occured error
 @since Version 1.0beta r85
 */
-(void)errorOccuredOnModelManager:(NSError*)err;






@end

/**
 Manage the local Context DB and dispatch events when modes are detected / lost.
 
 @warning No instance of VDARSDKController should be created manually. Use only the [VDARSDKController startSDK:withLicenseKey:] method to create a shared instance and to use it.
 */
@interface VDARSDKController : NSObject<VDARImageReceiver

>

/**
 Set the class to be instancied when the internal browser should be opened.
 
 This is set by default to nil, which corresponds to the default internal browser provided by the SDK.
 
 @since Version 2.1.1
 */
@property (nonatomic,copy) NSString *internalBrowserClassName;

/**
 The Context DB is loaded asynchronously at startup and therefore might not be available immediatly.
 This can known when a given function (add or remove) returns an error or by checking the appropriate property.
 In order to execute those operation when the DB is loaded, the operations can be added to the queue represented by this property.
 
 Once the DB is loaded, this property is not longer valid (set to nil).
 
 @see dbLoaded
 */
@property (nonatomic,readonly)  NSOperationQueue* afterLoadingQueue;


/**
 Defines whether or not the local Context DB is loaded or not. Any call related to the DB will fail if this property is not set to YES.
 */
@property (nonatomic,readonly)  BOOL dbLoaded;

/**
 Context detection delegates.
 */
@property (nonatomic,readonly)  NSMutableArray<NSObject<VDARSDKControllerDelegate>* >* detectionDelegates;

/**
 Sensor detection delegates.
 */
@property (nonatomic,readonly)  NSMutableArray<NSObject<VDARSDKSensorDelegate>* >* sensorDelegates;


/**
 The Context DB directory where all the data is stored.
 */
@property (nonatomic,readonly)  NSString* modelsDBDirectory;

/**
 * Display the debug overlay to locate the context properly even if the context has no annotation.
 * @return YES if the debug overlay is rendered, NO otherwise.
 * @since Version 2.0.0b2
 */
@property (nonatomic) BOOL displayModelDebugOverlay;

/**
 * The image sender which will send frame to the system. This is needed starting from version 2.0.11b
 * @since Version 2.0.11b
 */
@property (nonatomic,retain) NSObject<VDARImageSender>* imageSender;

/**
 The Remote API key generated by the AR Manager website.
 */
@property (nonatomic,readonly) NSString *remoteAPIKey;

/**
 The id of all contexts
 @since Version 5.0.0
 */
@property ( nonatomic, readonly) NSArray *contextIDs;

/**
 The id of all available contexts.
 
 An available context is defined as a context that is triggered by some sensors (i.e. a beacon) but that its content is not (yet) displayed on screen and require user approval to display it on screen.
 
 @since Version 5.1.0
 */
@property ( nonatomic, readonly) NSArray *availableContextIDs;

/**
 The list of all notifications that are pending.
 
 @since Version 5.1.0
 */
@property ( nonatomic, readonly) NSArray *pendingNotifications;

/**
 Prevent spurious notifications to be displayed.
 
 When set to YES, the notification list will never appear. Instead, the notification will be buffered and will be displayed as soon as the flag goes back to NO.
 
 @since Version 5.1.0
 */
@property (nonatomic)  BOOL preventNotifications;

/**
 The user device UUID
 
 @since Version 5.2.0
 */
@property (nonatomic, readonly) NSString* deviceUUID;

/**
 * Get the context of a given id
 * @param remoteID remote ID of the context
 * @return VDARContext
 * @since Version 5.0.0
 */
-(VDARContext*)getContext:(NSString*)remoteID;

/**
 * Load the context of a given id
 * @param remoteID remote ID of the context
 * @return YES if the context is successfully loaded
 * @since Version 5.0.0
 */
-(BOOL)loadContext:(NSString*)remoteID;

/**
 * Unload all the contexts in memory
 * @return Yes if the contexts are successfully unloaded
 * @since Version 5.0.0
 */
-(BOOL)unloadAllNonRootContexts;




/// ------------------------- ///
///  @name Code recognition
/// ------------------------- ///

/**
 When set to YES, code recognition is also performed while trying to recognize a VDARContext.
 
 Code recognition allows you to recognize different type of codes (1-D codes such as a barcodes or 2D codes such as QR Codes). Those codes can contains any information they can support. When a code is detected, the codesDetected: method of all the VDARSDKController delegates is called with the detected codes represented as VDARCode objects. You can then use the returned objects to read the codes data and act upon it.
 
 You can use the QR Code generator available in Vidinoti AR Manager under My Contents -> Code Generator to generate your own QR Codes.
 
 Several different special QR Codes are understood by the system. Those special QR codes actually contain a special URL. To be understood correctly, this special URL has to be of the following format:
 
	vdarsdk://<command>/<parameter 1>/.../<parameter n>
 
 The following commands are supported:
 
 1. Context Download QR Code (command = **mdl**)
 
 This special QR Code can trigger the download (a.k.a pre-fetch) of a VDARContext so that the VDARContext can be recognized afterhand. Those special QR codes should contains an URL of the following format:
 
	vdarsdk://mdl/<your Context Remote ID>
 
 For example, if you have a Context with the remote ID **fsgdg4t3few**, then you can create a QR code with this URL **vdarsdk://mdl/fsgdg4t3few** to autmatically trigger the download of the corresponding context.
 
 2. Tag synchrnoization request (command = **tsc**)
	
 This special QR Code can trigger the synchronization request (a.k.a pre-fetch) of several VDARContext corresponding to a given or multiple tags. Those special QR codes should contains an URL of the following format:
 
	vdarsdk://tsc/<tag 1>/<tag 2>/.../<tag n>
 
 For example, if you want to synchronize the local AR DB with all the contexts having the tags **hello** and **bye**, then you can create a QR code with this URL **vdarsdk://tsc/hello/bye** to autmatically trigger the download of the corresponding contexts.
 
 
 Note that the code recognition is automatically disabled when a VDARContext has been recognized and when it is tracked
 
 You can customize which codes can be recognized with the recognizableCodes property.
 
 @see recognizableCodes
 
 @since Version 1.1.0beta r230
 */
@property (nonatomic) BOOL enableCodesRecognition;

/**
 Select which types of codes should be recognized by the system.
 
 This variable should be an OR'ed mask of the constant defined in VDARCodeType. The mask can be built with the following values:
 
 - VDAR_CODE_TYPE_EAN2		   = GS1 2-digit add-on
 - VDAR_CODE_TYPE_EAN5		   = GS1 5-digit add-on
 - VDAR_CODE_TYPE_EAN8        = EAN-8
 - VDAR_CODE_TYPE_UPCE        = UPC-E
 - VDAR_CODE_TYPE_ISBN10      = ISBN-10 (from EAN-13).
 - VDAR_CODE_TYPE_UPCA        = UPC-A
 - VDAR_CODE_TYPE_EAN13       = EAN-13
 - VDAR_CODE_TYPE_ISBN13      = ISBN-13 (from EAN-13).
 - VDAR_CODE_TYPE_COMPOSITE   = EAN/UPC composite
 - VDAR_CODE_TYPE_I25         = Interleaved 2 of 5.
 - VDAR_CODE_TYPE_CODE39      = Code 39.
 - VDAR_CODE_TYPE_QRCODE      = QR Code.
 
 By default, the enabled codes when enableCodesRecognition is set to YES are **QR Codes**.
 
 @warning The more codes you enable, the slower the overall recognition will be.
 */
@property (nonatomic) VDARCodeType recognizableCodes;


/// ------------------------- ///
///  @name Notifications
/// ------------------------- ///

/**
 Enable push notifications support in the SDK.
 
 If set to YES, the application delegate has to forward the correct calls to VDARSDKController. This can be done this way in your AppDelegate:
 
	- (void)application:(UIApplication *)app didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)devToken {
		[[VDARSDKController sharedInstance] application:app didRegisterForRemoteNotificationsWithDeviceToken:devToken];
	}
 
	- (void)application:(UIApplication *)app didFailToRegisterForRemoteNotificationsWithError:(NSError *)err {
		[[VDARSDKController sharedInstance] application:app didFailToRegisterForRemoteNotificationsWithError:err];
	}
 
	- (void)application:(UIApplication *)app didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult result))handler {
		[[VDARSDKController sharedInstance] application:app didReceiveRemoteNotification:userInfo];
		handler(UIBackgroundFetchResultNoData);
	}

	- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
		// ...Your initialization code...
		// Enables the push notifications
		[VDARSDKController sharedInstance].enableNotifications=YES;
		// Forward the push notification if the application was not running when the notification has been received
		[[VDARSDKController sharedInstance] application:application didReceiveRemoteNotification:launchOptions];
		return YES;
	}

 By default, the value is NO.
 
 @since Version 3.1.0
 */
@property (nonatomic,getter=isNotificationsEnabled) BOOL enableNotifications;

/**
 When a notification arrives in a state where the application is open, the SDK will handle it and display an alert with the notification. You can disable this behaviour by setting this flag to NO.
 By default, it is set to YES.
 
 @since Version 5.1.0
 */
@property (nonatomic) BOOL enableDefaultNotificationController;

/**
 When a beacon notification arrives in a state where the application is open, the SDK will handle the notification and display it in a popup window.
 When the notification list is presented by default the SDK will show the nearby Beacons/GPS
 To show the list of the pending notification instead set this flag to YES.
 By default, it is set to NO.
 
 @since Version 5.3.0
 */
@property (nonatomic) BOOL enablePendingNotifications;

/**
 When a content needing the AR view to be opened is triggered, usually due to a beacon event, the app needs to open the AR view. By default, the SDK takes care of that. It opens the AR view if not already opened, or display the content directly if the AR view is open.
 
 You can prevent this functionnality by setting the flag to NO and implementing your own logic by listening to the VDARNotificationARViewRequested notification. An AR should be open as soon as the notification VDARNotificationARViewRequested is triggered.
 
 By default, it is set to YES.
 
 @since Version 5.1.0
 */
@property (nonatomic) BOOL enableDefaultARViewOpeningMechanism;

/**
 Defines the language that must be used for the online recognition. If it is not set, the device language must be used.
 
 @since Version 6.??
 */
@property (nonatomic, retain) NSString *cloudRecognitionLanguage;

/**
 Tell the PixLive SDK that the remote notification request has succeeded. The app delegate has to call this method when such an event is received.
 
 @param app The app triggering the event
 @param devToken The token for notification registration
 
 @since Version 3.1.0
 */
- (void)application:(UIApplication *)app didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)devToken;

/**
 Tell the PixLive SDK that the remote notification request has failed. The app delegate has to call this method when such an event is received.
 
 @param app The app triggering the event
 @param err The error explaining the failure
 
 @since Version 3.1.0
 */
- (void)application:(UIApplication *)app didFailToRegisterForRemoteNotificationsWithError:(NSError *)err;

/**
 Tell the PixLive SDK that a remote notification has been received. The app delegate has to call this method when such an event is received.
 
 @param application The app triggering the event
 @param userInfo The notification content. It is mandatory to pass this parameter.
 
 @warning If your app delegate implements the application:didReceiveRemoteNotification:fetchCompletionHandler: method, you should call [VDARSDKController application:didReceiveRemoteNotification:] within this method instead in the fetchCompletion less one.
 
 @since Version 3.1.0
 */
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo;

- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification;

- (void)application:(UIApplication *)application handleActionWithIdentifier:(NSString *)identifier forRemoteNotification:(NSDictionary *)userInfo completionHandler:(void(^)())completion;

/**
 enable pending notifications
 @param {BOOL}pendingNotificationsEnabled true to enable pending notification mode
 @since Version 5.3.0
 */
-(void)setEnablePendingNotifications:(BOOL)pendingNotificationsEnabled;

/**
 Present the last notification to the user
 @since Version 5.3.0
 */
-(void)presentLastNotification;

/**
 Present the in-SDK notification list to the user
 @since Version 5.1.0
 */
-(void)presentNotificationsList;

/**
 Present the in-SDK notification list to the user with nearby GPS points
 @since Version 5.3.0
 */
-(void)presentNearbyList;

/**
 Present the in-SDK notification list to the user with nearby GPS points
 @since Version 5.3.0
 */
-(void)presentNearbyListForLat:(float) myLat lon:(float) myLon;;

/**
 Present the in-SDK notification list to the user with nearby GPS points
 @since Version 5.3.0
 */
-(void)refreshNearbyListForLat:(float) myLat lon:(float) myLon;

/// ------------------------- ///
///  @name Geo Points
/// ------------------------- ///

/**
 Return YES if the app contain GPS points, NO otherwise
 
 @since Version 5.3.0
 
 @return YES if the app contain GPS points, NO otherwise
 */
-(BOOL)isContainingGPSPoints;

/**
 The distance bewteen to GPS points in meter
 @param lat1 latitude of point 1
 @param lon1 longitude of point 1
 @param lat2 latitude of point 2
 @param lon2 longitude of point 2
 @since Version 5.3.0
 */
-(float)computeDistanceBetweenGPSPointsOfLat1:(float) lat1 lon1:(float) lon1 lat2:(float) lat2 lon2:(float) lon2;

/**
 The list of nearby GPS points
 @param myLat current latitude
 @param myLon current longitude
 @since Version 5.3.0
 */
-(NSArray<VDARGPSPoint*>*)getNearbyGPSPointsfromLat:(float) myLat lon:(float) myLon;

/**
 Returns the new nearby GPS points. That means the GPS points that are close to the given position and that have not been already detected.
 Once a GPS point is returned, it is marked as detected and won't be returned at the next call of this method.
 @param myLat the GPS latitude of the current position
 @param myLon the GPS longitude of the current position
 @param detectionInterval the number of seconds between 2 detections (i.e. after a detection, we must wait `detectionInterval` before receving again the GPS point). A value of 0 means infinite (i.e. a GPS point is detected only once)
 @param maxDetectionRadius the upper limit for the detection radius in meters. A value of 0 or below means no limit.
 */
-(NSArray<VDARGPSPoint*>*)getNewNearbyGPSPointsfromLat:(float) myLat lon:(float) myLon detectionInterval:(long) detectionInterval maxDetectionRadius: (float) maxDetectionRadius;

/**
 Clears the list of GPS points that have been detected.
 @see getNewNearbyGPSPointsfromLat:lon:detectionInterval:maxDetectionRadius:
 */
-(void)resetDetectedGPSPoints;

/**
 The list of GPS points in BoundingBox
 @param minLat latitude of the lower left corner
 @param minLon longitude of the lower left corner
 @param maxLat latitude of the uper right corner
 @param maxLon longitude of the uper right corner
 @since Version 5.3.0
 */
-(NSArray<VDARGPSPoint*>*)getGPSPointsInBoundingBoxOfMinLat:(float) minLat minLon:(float) minLon maxLat:(float) maxLat maxLon:(float) maxLon;

/**
 Starts the detection of nearby GPS points. Register a delegate to VDARNearbyGPSController to receive detected GPS points.
 Activate local notification by calling #startGPSNotifications
 */
-(void)startNearbyGPSDetection;

/**
 Starts the detection of new nearby GPS points
 @param minDistance the minimum distance difference between between two location updates
 @param detectionInterval the number of seconds between 2 detections (i.e. after a detection, we must wait `detectionInterval` before receving again the GPS point). A value of 0 means infinite (i.e. a GPS point is detected only once)
 @param maxRadius the upper limit for the detection radius in meters. A value of 0 or below means no limit.
 */
-(void)startNearbyGPSDetectionWithMinDistance:(double) minDistance detectionInterval:(long) detectionInterval maxDetectionRadius:(float) maxRadius;

/**
 Stops the nearby GPS point detection and clear the list of detected points.
 */
-(void)stopNearbyGPSDetection;

/**
 Calls this from your AppDelegate to enable the correct handling of GPS local notifications.
 */
-(void)enableGPSNotifications;

/**
 Add local notifications when a nearby GPS point is detected and the application is in background
 Requires #startNearbyGPSDetection
 */
-(void)startGPSNotifications;

/**
 Disable local notifications when a user enters a nearby GPS point zone.
 */
-(void)stopGPSNotifications;


/// ------------------------- ///
///  @name Beacons
/// ------------------------- ///

/**
 Return YES if the app contain beacons, NO otherwise
 @since Version 6.0.0
 @return YES if the app contain beacons, NO otherwise
 */
-(BOOL)isContainingBeacons;

/**
 Return the list of context IDs of nearby beacons.
 @since Version 5.3.0
 */
-(NSArray<NSString*>*)getNearbyBeacons;

/**
 * Enables/disables the nearby requirement dialog asking for bluetooth/localisation requirement
 * If enabled, a dialog will ask the user to enabled the required settings for bluetooth and
 * localisation when the SDK start to scan for beacons.
 * By default, this is enabled.
 *
 * @param {BOOL}nearbyRequirementDialogEnabled true for enabling the dialog, false otherwise
 * @since Version 6.0.0
 */
-(void)setEnableNearbyRequirementDialog:(BOOL)nearbyRequirementDialogEnabled;

/**
 Return localization authorization status  and bluetooth status
 @since Version 5.3.0
 @return NSDictinary with keys @"authorizationStatus" (@"unknow",@"disabled",@"enabled",@"enabled_in_use") and @"bluetoothStatus" (@"unknow",@"disabled",@"unsupported",@"enabled")
 */
-(NSDictionary*)getNearbyStatus;


/// ------------------------- ///
///  @name Bookmarks
/// ------------------------- ///

/**
 Defines whether the application should display a button for adding a
 bookmark when some content is displayed (content with web view {@link VDARModalWebView}).
 
 For content within the AR view, it is up to the application developper to implement it.
 
 @since Version 5.2.5
 */
@property (nonatomic,getter=isBookmarksEnabled) BOOL enableBookmarks;

/**
 The list of context IDs that have been bookmarked so far.
 
 @since Version 5.2.5
 */
@property (nonatomic,readonly) NSArray<NSString*>* bookmarks;

/**
 Add a bookmark for the specified context
 
 @param contextId The ID of the Context to add a bookmark for
 
 @since Version 5.2.5
 */
-(void)addBookmark:(NSString*)contextId;

/**
 Remove the bookmark for the specified context
 
 @param contextId The ID of the Context to remove the bookmark for
 
 @since Version 5.2.5
 */
-(void)removeBookmark:(NSString*)contextId;

/**
 Return YES if the context identified by contextId is already bookmarked, NO otherwise
 
 @since Version 5.2.5
 
 @return YES if already bookmarked, NO otherwise
 */
-(BOOL)isBookmarked:(NSString*)contextId;

/// ----------------------------- ///
///  @name Initialization methods
/// ----------------------------- ///


/**
 Return the shared instance of the VDARSDKController.
 @warning The VDARSDKController should have been already started using [VDARSDKController startSDK:withLicenseKey:] method.
 @return the shared instance of the VDARSDKController
 @since 5.0.0
 @see startSDK:withLicenseKey:
 */
+ (instancetype)sharedInstance;

/**
 Start the shared instance of the VDARSDKController. Calling it twice will return nil.
 
 @param modelDBDir The directory in which all the contexts and the context DB will be stored. Should be a user-writable directory (it will be created if it does not yet exists)
 @param licenseKey The API License key generate by Vidinoti AR Manager. Get it from https://armanager.vidinoti.com by logging in into your account.
 @return The VDARSDKController instance if the initilization happened correctly, nil otherwise.
 @since Version 3.2.0
 */
+ (instancetype)startSDK:(NSString*)modelDBDir withLicenseKey:(NSString*)licenseKey;




///-----------------------------------------------------///
/// @name Other methods
///-----------------------------------------------------///


/**
 Set the current device orientation. This is necessery to be able to recognize images when the device is not in portrait mode.
 @param d The orientation of the device.
 
 @since Version 2.0.0beta r251
 */
-(void)setDeviceOrientation:(UIDeviceOrientation)d;


/**
 Release the memory used by not-detected-but-loaded contexts. Level is for future use.
 
 @param level Pass 2 to this parameter. Reserved for future use.
 @since Version 1.0beta r85
 */
-(void)releaseMemory:(int)level;

/**
 Save the contexts and DB to disk.
 
 This method generate a VDARNotificationContextListChanged notification if changes have been made to the DB and saved to disk.
 
 @since Version 1.0beta r85
 */
- (void)save;



/**
 Open the given URL in the internal browser embedded in the SDK (instead of opening up the device browser and switching applications).
 @param url The URL to open in the internal browser
 @since Version 2.0.2
 */
-(void)openURLInInternalBrowser:(NSURL*)url;

/**
 Open the given URL in the internal browser embedded in the SDK (instead of opening up the device browser and switching applications).
 @param url The URL to open in the internal browser
 @param hideToolbar true if the must top toolbar must be hidden (not completely hidden but translucent)
 @param animated true if an animation must be played when opening the view
 */
-(void)openURLInInternalBrowser:(NSURL*)url hideToolbar:(BOOL)hideToolbar animated:(BOOL)animated;

/**
 * Open the give local file URL in the internal browser embedded in the SDK. This method must be used
 * for internal file URL which are sandboxed. (e.g. HTML file bundled in the app)
 * @param url the file URL
 * @param hideToolbar true if the must top toolbar must be hidden (not completely hidden but translucent)
 * @param animated true if an animation must be played when opening the view
 */
-(void)openFileURLInInternalBrowser:(NSString*)url hideToolbar:(BOOL)hideToolbar animated:(BOOL)animated;

/**
 Open the ARKit scanner with integrated webview
 @param url the URL that must be loaded by the web view
 */
-(void)playAR:(NSString*)url;

/**
 The preview of the passed context is shown. The context image has to be loaded from the network, therefore the completion block can be used in order to know when the fetch is completed and the preview is started.
 
 Pass nil for the context argument to stop the preview.
 
 @param contextId The id of context to preview or nil to stop the currently playing preview.
 @param completion The block that will be called when the preview is started. Can be nil.
 @return YES in case of succcess, NO otherwise.
 @since Version 5.0
 */
-(BOOL)previewContext:(NSString*)contextId completionBlock:(void (^)(BOOL err))completion;

/**
 This method will force the system to stop and hide anything displayed in the AR view such as context, content or annotations.
 
 @since Version 5.2.5
 */
-(void)unloadAll;


/**
 Return a string describing the VDARSDK version.
 @return A string uniquely identifying the version of this VDARSDK instance
 @since Version 1.0.1rc1 r160
 */
+(NSString*)VDARSDKVersion;


/**
 Return YES if the current device can run the Vidinoti AR SDK.
 
 You should call this method before attempting to start the VDARSDKController, otherwise it might result in crashes.
 @return YES if the device is supported, NO otherwise.
 @since Version 2.0.0GM
 */
+(BOOL) isCapableOfRunningVDARSDK;

/**
 Return the device type on which the SDK is running.
 
 @return The type of device represented as a UIDevicePlatform on which the SDK is running.
 @since Version 2.1.2
 */
+ (UIDevicePlatform) platformType;

/**
 Disables or enables a vision context. If the vision context is ignored, it won't be recognized,
 even if it is present locally on the device. By default, all vision contexts are enabled (i.e. not ignored)
 
 @since Version 6.1.4
 */
-(void)setVisionContext:(NSString*)contextId ignored:(BOOL)ignored;

/**
 Unloads and removes the context.
 
 @param contextId The id of context to remove.
 @since Version 6.1.5
 */
-(void)removeContext:(NSString*)contextId;

/**
 Disables the context. The context remains local but won't be detected (its sensors are unregistered)

 @param contextId The id of context to disable.
 @since Version 6.5.12
 */
-(void)disableContext:(NSString*)contextId;

/**
 Enables the context after being disabled (see disableContext).

 @param contextId The id of context to enable.
 @since Version 6.5.12
 */
-(void)enableContext:(NSString*)contextId;

/**
 Disables all the contexts. The context remains on the device but their sensors are unregistered.
 @since Version 6.5.12
 */
-(void)disableContexts;

/**
 * Enables all contexts belonging to specific tags. Ensure that the mapping Context <-> Tag exists otherwise
 * no context will be enabled. The mapping Context-Tag must be retrieved using the VDARRemoteController
 * (see syncTagContexts)
 * @param tags a list of tag names. The context to enable must have all of the given tags (logical AND)
 */
-(void)enableContextsWithTags:(NSArray*)tags;

/**
 Allows changing the license key when the SDK is already running.
 The new license key will be validated against the server.
 @param licenseKey the new license key.
 */
-(void)setLicenseKey:(NSString*)licenseKey;

/**
 Sets the language of the SDK interface. By default, the SDK uses the phone language.
 If this method is called, it uses the given language instead.
 Call this method with null, for cancelling the forced language.
 @param languageCode the language code (2 lowercase characters, e.g. "fr")
 */
-(void)forceLanguage:(NSString*)languageCode;

/**
 When set to YES, the image and code recognition is paused. The camera can continue to
 receive images but the image frames won't be processed by the vision system.
 */
@property (nonatomic) BOOL pauseVisionRecognition;

@end



#endif
