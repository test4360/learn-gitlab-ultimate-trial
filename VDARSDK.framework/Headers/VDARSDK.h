//
// VDARSDK.h
// PixLive SDK
// Version 7.3.0-9064fd9
//
// Built on 2021-06-29 at 21:00:40
//
// https://www.vidinoti.com
//
// Copyright 2011-2015 Vidinoti SA. All rights reserved.
//
// Any distribution of this file or any part of the PixLive SDK without
// Vidinoti SA express consentment is forbidden.
//

#ifndef VIDINOTI_AR_SDK
#define VIDINOTI_AR_SDK

#include <VDARSDK/VDARSDKController.h>
#include <VDARSDK/VDARContext.h>
#include <VDARSDK/VDARRemoteController.h>
#include <VDARSDK/VDARLiveAnnotationViewController.h>
#include <VDARSDK/VDARPrior.h>
#include <VDARSDK/VDARContextPrior.h>
#include <VDARSDK/VDARTagPrior.h>
#include <VDARSDK/VDARTourPrior.h>
#include <VDARSDK/VDARLocalizationPrior.h>
#include <VDARSDK/VDARApplicationPrior.h>
#include <VDARSDK/VDARIntersectionPrior.h>
#include <VDARSDK/VDARUserEmailPrior.h>
#include <VDARSDK/VDARAnnotationView.h>
#include <VDARSDK/VDARCode.h>
#include <VDARSDK/VDARModalWebViewController.h>
#include <VDARSDK/VDARCameraImageSource.h>
#include <VDARSDK/VDARSensor.h>
#include <VDARSDK/VDARVisionSensor.h>
#include <VDARSDK/VDARIBeaconSensor.h>
#include <VDARSDK/VDARVidiBeaconSensor.h>
#include <VDARSDK/VDARNearbyGPSController.h>

#ifndef VDAR_INPUT_IMAGE_WIDTH
#define VDAR_INPUT_IMAGE_WIDTH 640
#endif

#ifndef VDAR_INPUT_IMAGE_HEIGHT
#define VDAR_INPUT_IMAGE_HEIGHT 480
#endif



#endif
