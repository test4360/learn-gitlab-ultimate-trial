//
//  VDARUserEmailPrior.h
//  VDARSDK
//
//  Created by Johan Leuenberger on 15.04.19.
//


#import <Foundation/Foundation.h>
#import <VDARSDK/VDARPrior.h>

/**
 Represent a user email prior information needed for model fetching requests.
 When a request is sent to Vidinoti AR servers to fetch multiple models, one can specify under which condition a model should be returned. Such a condition can be, in that case, a user email. Sending this prior will return the models created by the user having the given email address
 */
@interface VDARUserEmailPrior : VDARPrior 


/**
 Initalize a new user email prior with the given email.
 @param userEmail the email of the user that created the models
 @return The newly created instance of the user email prior
 */
-(id)initWithUserEmail:(NSString*)userEmail;


/**
 The user email
 */
@property (nonatomic,retain) NSString* userEmail;

/**
 Create and initalize a new prior with the given user email
 @param userEmail the email of the user who created the models
 @return The newly created instance of the user email prior
 */
+(VDARUserEmailPrior*)userWithEmail:(NSString*)userEmail;

@end
