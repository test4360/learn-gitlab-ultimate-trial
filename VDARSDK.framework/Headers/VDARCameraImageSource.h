//
// VDARCameraImageSource.h
// PixLive SDK
// Version 7.3.0-9064fd9
//
// Built on 2021-06-29 at 21:00:40
//
// https://www.vidinoti.com
//
// Copyright 2011-2015 Vidinoti SA. All rights reserved.
//
// Any distribution of this file or any part of the PixLive SDK without
// Vidinoti SA express consentment is forbidden.
//

#import <Foundation/Foundation.h>
#import <CoreMedia/CoreMedia.h>
#import <AVFoundation/AVFoundation.h>

#import <VDARSDK/VDARSDK.h>

#ifdef __APPLE__
#include "TargetConditionals.h"
#endif

/**
 Implementation to provide images to PixLive SDK from the device back-side camera.
 
 In the simulator it also provides a static images that can be configured through the [VDARCameraImageSource cameraImageSource] property.
 */
@interface VDARCameraImageSource : NSObject<VDARImageSender

>

/** The receiver of the camera frames */
@property (nonatomic,assign) id<VDARImageReceiver> imageReceiver;

/**
 The frame rate of the camera.
 
 Can be adjusted to a lower value if the device is not powerful enough to keep up the framerate.
 Usually a value betweek 20-25 (fps) works correctly.
 */
@property (nonatomic) unsigned int frameRate;

/**
 Indicate whether or not the camera is running and capturing frames.
 */
@property (nonatomic, getter = isRunning, readonly) BOOL running;

/**
 URL pointing to a valid JPG image used to display in the simulator. By default it's a beatles image.
 */
@property (nonatomic, retain) NSString* cameraImageSource;

/**
 Access to the camera object used to produce frames
 @since 5.2.0
 */
@property (nonatomic, readonly) AVCaptureDevice *cameraDevice;

/** Start the image stream */
-(void)startImageStream;

/** Strop the image stream */
-(void)stopImageStream;



@end
