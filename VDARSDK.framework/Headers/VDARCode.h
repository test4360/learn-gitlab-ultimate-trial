//
// VDARCode.h
// PixLive SDK
// Version 7.3.0-9064fd9
//
// Built on 2021-06-29 at 21:00:40
//
// https://www.vidinoti.com
//
// Copyright 2011-2015 Vidinoti SA. All rights reserved.
//
// Any distribution of this file or any part of the PixLive SDK without
// Vidinoti SA express consentment is forbidden.
//

#import <Foundation/Foundation.h>

#include "VDARCodeType.h"

/**
 A code detected by VDARSDKController.
 */
@interface VDARCode : NSObject

/** The type of code recognized, can be only one of the value of the VDARCodeType enum. */
@property (nonatomic,readonly) VDARCodeType codeType;

/** When this property is set to YES, the code has already been treated by VDARSDK and the proper action has been done (such as downloading a VDARModel). You should safely ignore this code in your processing routines.
 */
@property (nonatomic,readonly) BOOL isSpecialCode;

/** The code data represented as a string (usually readable string as codes usually encode readable data). */
@property (nonatomic,readonly) NSString* codeData;


/**
 Return true if the receiver is equal to c.
 
 A code is defined to be equal if it is the same type (codeType) and has the same data (codeData).
 
 @param c The code to compare to
 @return YES if equal, NO otherwise.
 */
-(BOOL)isEqualToCode:(VDARCode*)c;




@end
