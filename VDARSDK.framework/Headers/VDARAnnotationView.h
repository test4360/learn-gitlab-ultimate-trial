//
// VDARAnnotationView.h
// PixLive SDK
// Version 7.3.0-9064fd9
//
// Built on 2021-06-29 at 21:00:40
//
// https://www.vidinoti.com
//
// Copyright 2011-2015 Vidinoti SA. All rights reserved.
//
// Any distribution of this file or any part of the PixLive SDK without
// Vidinoti SA express consentment is forbidden.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import <OpenGLES/ES3/gl.h>
#import <OpenGLES/ES3/glext.h>




/**
 Class used to display the augmented reality view. It should not be used alone but only with a VDARLiveAnnotationViewController.
 
 @warning Creating an instance of this class manually will result in a crash of the application.
 */
@interface VDARAnnotationView : UIView



/**
 * The speed of animation displayed in the transition from dark screen to non-dark screen. The animation is disabled if the value is set as 0.
 * @return a float value (default: 0).
 * @since Version 3.1.0
 */
@property (nonatomic) float animationSpeed;

/**
 * The brightness of camera view drops by half if the value is true.
 * @return true if the dark screen mode is turned on.
 * @since Version 3.1.0
 */
@property (nonatomic) BOOL darkScreen;

/**
 The running / rendering state of the view
 @return True if the current view is rendering its content, false otherwise.
 @since Version 5.1.0
 */
@property (nonatomic, readonly, getter=isRendering) BOOL rendering;

/**
 Tells the view to wait that time before starting the camera. Useful to make the camera starting time delayed so that the screen stays black until the content is opened.
 
 @since 5.1.0
 */
@property (nonatomic) CFTimeInterval cameraStartingDelay;

/**
 Tells the view not to start the camera to prevent the display of camera frames
 
 Default: NO
 
 @since 5.2.5
 */
@property (nonatomic) BOOL preventCameraStart;

/**
 * Capture screen shot of the AR view at the time the method is executed.
 */
-(UIImage*)captureScreenshot;

@end


