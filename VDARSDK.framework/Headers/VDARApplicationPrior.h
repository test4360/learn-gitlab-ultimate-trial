//
// VDARApplicationPrior.h
// PixLive SDK
// Version 7.3.0-9064fd9
//
// Built on 2021-06-29 at 21:00:40
//
// https://www.vidinoti.com
//
// Copyright 2011-2015 Vidinoti SA. All rights reserved.
//
// Any distribution of this file or any part of the PixLive SDK without
// Vidinoti SA express consentment is forbidden.
//

#import <Foundation/Foundation.h>

#import <VDARSDK/VDARPrior.h>

/**
 Represent an application prior. Models selected for this app will be downloaded. Note that this prior is always included when doing a synchronization.
 @since Version 2.1.2
 */
@interface VDARApplicationPrior : VDARPrior 

@end
