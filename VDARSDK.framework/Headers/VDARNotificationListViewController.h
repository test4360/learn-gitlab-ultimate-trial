//
// VDARNotificationListViewController.h
// PixLive SDK
// Version 7.3.0-9064fd9
//
// Built on 2021-06-29 at 21:00:40
//
// https://www.vidinoti.com
//
// Copyright 2011-2015 Vidinoti SA. All rights reserved.
//
// Any distribution of this file or any part of the PixLive SDK without
// Vidinoti SA express consentment is forbidden.
//

#import <UIKit/UIKit.h>

@class VDARNotificationListViewController;

@protocol VDARNotificationListDelegate <NSObject>

-(void)notificationListDidClose:(VDARNotificationListViewController*)list;

@end

@interface VDARNotificationListViewController : UITableViewController

typedef enum {
    NEARBY,
    PENDING_NOTIFICATIONS
} NotificationListBeaconMode;

@property (nonatomic, readonly) BOOL hidden;
@property (nonatomic) BOOL showNotifView;
@property (nonatomic, assign) NSObject<VDARNotificationListDelegate>* delegate;

-(void)showPendingNotifications;
-(void)showNearbyForLat:(float)myLat lon:(float)myLon GPSEnabled:(BOOL)GPSisEnabled;
-(void)refreshNearbyForLat:(float)myLat lon:(float)myLon;
@end
