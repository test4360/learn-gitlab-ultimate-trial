//
// VDARVidiBeaconSensor.h
// PixLive SDK
// Version 7.3.0-9064fd9
//
// Built on 2021-06-29 at 21:00:40
//
// https://www.vidinoti.com
//
// Copyright 2011-2015 Vidinoti SA. All rights reserved.
//
// Any distribution of this file or any part of the PixLive SDK without
// Vidinoti SA express consentment is forbidden.
//

#import <VDARSDK/VDARSDK.h>

/**
 Represent a vision sensor that have a trigger state corresponding to wether or not the device is in the proximity of the VidiBeacon.
 
 The RSSI value can also be obtained through the value updates.
 
 @since 5.2.0
 */
@interface VDARVidiBeaconSensor : VDARSensor
@property (nonatomic) float rssi;
@end
