//
// VDARIntersectionPrior.h
// PixLive SDK
// Version 7.3.0-9064fd9
//
// Built on 2021-06-29 at 21:00:40
//
// https://www.vidinoti.com
//
// Copyright 2011-2015 Vidinoti SA. All rights reserved.
//
// Any distribution of this file or any part of the PixLive SDK without
// Vidinoti SA express consentment is forbidden.
//

#import <Foundation/Foundation.h>
#import <VDARSDK/VDARSDK.h>

/**
 Represent a combination of two or more priors where the result set is the intersection of the result set of each prior in the combination.
 
 @since 5.2.0
 */
@interface VDARIntersectionPrior : VDARPrior

/**
 Create a new intersection prior with the differents nested-priors.
 
 @param priors Array of the different nested priors
 
 @return A new intersection prior
 
 */
-(id)initWithPriors:(NSArray<VDARPrior*>*)priors;


/*
 Array of priors represented by this intersection priors.
 */
@property (nonatomic,retain) NSArray* priors;

@end
