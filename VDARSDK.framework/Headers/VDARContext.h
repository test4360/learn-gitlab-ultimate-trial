//
// VDARContext.h
// PixLive SDK
// Version 7.3.0-9064fd9
//
// Built on 2021-06-29 at 21:00:40
//
// https://www.vidinoti.com
//
// Copyright 2011-2015 Vidinoti SA. All rights reserved.
//
// Any distribution of this file or any part of the PixLive SDK without
// Vidinoti SA express consentment is forbidden.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>




#ifndef VDARContext_H
#define VDARContext_H


/** The block which will be called after that the context thumbnail is downloaded
 @param result A UIImage representing the context thumbnail or nil if an error happened.
 @param err If an error occured, the err object represent it, otherwise this parameter is set to nil.
 
 @since Version 2.0.3
 */
typedef void (^VDARContextImageCompletionHandler)(UIImage* result, NSError *err);

/** The block which will be called after that the context file is downloaded
 @param result A NSString representing the path to the file which has been downloaded or nil if an error happened.
 @param err If an error occured, the err object represent it, otherwise this parameter is set to nil.
 
 @see [VDARContext downloadContextFile:progress:completion]
 
 @since Version 3.0.3
 */
typedef void (^VDARContextFileDownloadCompletionHandler)(NSString* filepath, NSError *err);

/** The block which will be called during the file is downloaded.
 @param filename A NSString representing the filename of the file being downloaded
 @param prc The completion percentage (between 0 and 1) of the downloaded. 1 being the file has been completly downloaded.
 
 @see [VDARContext downloadContextFile:progress:completion]
 
 @since Version 3.0.3
 */
typedef void (^VDARContextFileDownloadProgressHandler)(NSString* filename, float prc);

/**
 Object representing an AR Context.
 
 This class provided properties in order to retrieve some information about the detected context. Furthermore it can inform the application the state at which the context is currently at (see contextState).
 */
@interface VDARContext : NSObject

/** 
 The name of the context as entered in ARManager. Can be nil if no name is given.
 @since Version 1.0beta r85
 */
@property (nonatomic,copy) NSString * name;

/**
 The last time at which this context was modified
 @since Version 1.0beta r85
 */
@property (nonatomic, readonly) NSDate * lastmodif;

/**
 If the context is remote, this property stores the remote ID of the context. This property is set to nil if the context is only local.
 @since Version 1.0beta r85
 */
@property (nonatomic, readonly) NSString * remoteID;

/**
 The description of context
 @since Version 5.0
 */
@property (nonatomic,readonly) NSString * contextDescription;

/**
 The notification title currently associated with this context. Note that it can change throughout app life. Nil if no title is set.
 @since Version 5.1
 */
@property (nonatomic,readonly) NSString * notificationTitle;

/**
 The notification message currently associated with this context. Note that it can change throughout app life. Nil if no message is set.
 @since Version 5.1
 */
@property (nonatomic,readonly) NSString * notificationMessage;

/**
 The web URL to the thumbnail of this context image.
 @since Version 5.1.7
 */
@property (nonatomic,readonly) NSURL * imageThumbnailURL;

/**
 The web URL to the full resolution image of this context.
 @since Version 5.1.7
 */
@property (nonatomic,readonly) NSURL * imageHiResURL;

/**
 A list of NSString representing the different filenames of the files attached to this context.
 
 The different files are specified in the Metadata section of PixLive Maker. The [VDARContext downloadContextFile:progress:completion:] method can be used to asynchronously retrieve one of this file by providing its filename.
 
 @since Version 5.0
 */
@property (nonatomic, readonly) NSArray* contextFiles;

/**
 Asynchronously download (if needed) the context file identified by its filename.
 
 If the file has already been downloaded, the file is cached and the completion callback is called within the next run loop iteration.
 
 The filename has to be in the array VDARContext.contextFiles otherwise a false value is returned.
 
 @warning Note this is **not** safe to call this method twice, without waiting for the first call to finish.
 
 The different blocks are called on the main thread.
 
 @param filename The filename of the file to download.
 @param progressBlock The block that is called to update the caller of the progression of the file download
 @param completionBlock The completion block to call when the file is available
 @return YES if the filename is correct and if the download can start, NO otherwise.
 
 @since Version 5.0
 */
-(BOOL)downloadContextFile:(NSString*)filename progress:(VDARContextFileDownloadProgressHandler)progressBlock completion:(VDARContextFileDownloadCompletionHandler)completionBlock;



/**
 Asynchronously return the context image thumbnail (if any).
 
 The block is called on the main thread when the thumbnail is available.
 @param completionBlock The completion block to call when the thumbnail is available
 @return YES in case of success, NO if for some reason the thumbnail cannot be retrieved.
 
 @since Version 5.0
 */
-(BOOL)contextImageThumbnailAsynchronously:(VDARContextImageCompletionHandler)completionBlock;

/**
 Asynchronously return the context image at size which can be used for a full screen preview.
 
 The block is called on the main thread when the image is available.
 @param completionBlock The completion block to call when the image is available
 @return YES in case of success, NO if for some reason the thumbnail cannot be retrieved.
 
 @since Version 5.0
 */
-(BOOL)contextImageAsynchronously:(VDARContextImageCompletionHandler)completionBlock;

/**
 Activate the context's content to the user. This is typically done when a context post a notification and the user accept it, while the application is in foreground.
 
 @since Version 5.1
 */
-(void)activate;

/**
 Ignore the context's notification and mark as if the content has been played.
 
 @since Version 5.1
 */
-(void)ignore;

/**
 Stops the context when it is running
 */
-(void)stop;

/**
 Execute and evaluate the JS string in the Context AnnotationJS system.
 
 @param jsString The Javascript String to evaluate
 
 @return YES if it succeeeded, NO otherwise. NO can occur if the context is not loaded and executed in memory.
 
 @since Version 5.1.9.4
 
 */
-(BOOL)executeJSInContext:(NSString*)jsString;



@end

#endif //VDARContext_H
