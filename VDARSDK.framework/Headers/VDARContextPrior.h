//
// VDARContextPrior.h
// PixLive SDK
// Version 7.3.0-9064fd9
//
// Built on 2021-06-29 at 21:00:40
//
// https://www.vidinoti.com
//
// Copyright 2011-2015 Vidinoti SA. All rights reserved.
//
// Any distribution of this file or any part of the PixLive SDK without
// Vidinoti SA express consentment is forbidden.
//

#import <Foundation/Foundation.h>
#import <VDARSDK/VDARPrior.h>

/**
 Represent a context prior information needed for model fetching requests.
 
 When a request is sent to Vidinoti AR servers to fetch multiple models, one can specify under which condition a model should be returned. Such a condition can be, in that case, a context ID. Sending this prior will return the specific model with the given context ID.
 */
@interface VDARContextPrior : VDARPrior 


/**
 Initalize a new context prior with the given context ID.
 @param contextID the public context ID
 @return The newly created instance of the context prior
 */
-(id)initWithContextID:(NSString*)contextID;


/**
 The context public ID
 */
@property (nonatomic,retain) NSString* contextID;

/**
 Create and initalize a new prior with the given contextID
 @param contextID the public context ID
 @return The newly created instance of the context prior
 */
+(VDARContextPrior*)contextWithID:(NSString*)contextID;

@end
