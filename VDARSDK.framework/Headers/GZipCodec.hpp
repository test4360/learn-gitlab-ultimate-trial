//
//  GZipCodec.hpp
//  RecognitionTests
//
//  Created by Internship on 31/10/16.
//  Copyright © 2016 Vidinoti. All rights reserved.
//

#ifndef GZIPCODEC_H_INCLUDED
#define GZIPCODEC_H_INCLUDED

#include <string>
#include "BRIEFPoint.h"
#include <vector>

//
// Very simple GZip Codec
//

namespace GZipCodec
{
    // GZip Compression
    // @param data - the data to compress (does not have to be string, can be binary data)
    // @param compressedData - the resulting gzip compressed data
    // @param level - the gzip compress level -1 = default, 0 = no compression, 1= worst/fastest compression, 9 = best/slowest compression
    // @return - true on success, false on failure
    bool Compress(const std::string& data, std::string& compressedData, int strategy, int level=-1);
    
    // GZip Decompression
    // @param compressedData - the gzip compressed data
    // @param data - the resulting uncompressed data (may contain binary data)
    // @return - true on success, false on failure
    bool Uncompress(const std::string& compressedData, std::string& data);
    
    void sortKeypointsHammingDistance(std::vector<BRIEFKeypointNoOrientation> &keypoints, bool findFirstKeypoint);
    void residualCoding(std::vector<BRIEFKeypointNoOrientation> &keypoints);
}

#endif // GZIPCODEC_H_INCLUDED

