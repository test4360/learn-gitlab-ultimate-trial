//
// VDARGPSPoint.h
// PixLive SDK
// Version 7.3.0-9064fd9
//
// Built on 2021-06-29 at 21:00:40
//
// https://www.vidinoti.com
//
// Copyright 2011-2015 Vidinoti SA. All rights reserved.
//
// Any distribution of this file or any part of the PixLive SDK without
// Vidinoti SA express consentment is forbidden.
//

#ifndef VDARGPSPOINT_H
#define VDARGPSPOINT_H

#import <Foundation/Foundation.h>

@interface VDARGPSPoint : NSObject

@property (nonatomic, retain) NSString *contextId;
@property (nonatomic, retain) NSString *label;
@property (nonatomic, retain) NSString *category;
@property (nonatomic) float lat;
@property (nonatomic) float lon;
@property (nonatomic) float detectionRadius;
@property (nonatomic) float distanceFromCurrentPos;
@end

#endif
