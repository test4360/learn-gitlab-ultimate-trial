//
// VDARSensor.h
// PixLive SDK
// Version 7.3.0-9064fd9
//
// Built on 2021-06-29 at 21:00:40
//
// https://www.vidinoti.com
//
// Copyright 2011-2015 Vidinoti SA. All rights reserved.
//
// Any distribution of this file or any part of the PixLive SDK without
// Vidinoti SA express consentment is forbidden.
//

#import <Foundation/Foundation.h>

/**
 Represent a sensor that have a trigger state and some associated value. This is an abstract class and should not be instantiated.
 
 @since 5.2.0
 */
@interface VDARSensor : NSObject
@property (nonatomic,readonly) NSString* sensorId;
@property (nonatomic,readonly) NSString* type;
@property (nonatomic) BOOL triggered;
-(id)initWithId:(NSString*)sId;
@end
