//
//  VDARNearbyGPSController.h
//  VDARSDK
//
//  Created by Johan Leuenberger on 25.03.20.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "VDARGPSPoint.h"

/**
 Controller that manages the detection of nearby GPS points. A location manager is started with
 start/stopNearbyGPSDetection. The detected nearby GPS points are sent to the delegate if any.
 If the notification are enabled (i.e. the enableNotifications property is true), a local notification is
 posted when the user enters a GPS zone and the application is in background. By default,
 the local notifications are not displayed if the application is in foreground.
 Attention: The application must enable the capability "Background modes > Location updates" as
 the location manager runs in background.
 Call VDARSDKController#enableGPSNotification in your AppDelegate for handling the click on
 the notifications.
 */
@class VDARNearbyGPSController;

/**
 The delegate can be used to receive the detected GPS points
 */
@protocol VDARNearbyGPSControllerDelegate <NSObject>

/**
 Called each time that a GPS nearby check is executed. The method is called even if no new nearby GPS point is detected (gpsPoints is then empty).
 @param controller the current VDARNearbyGPSController
 @param gpsPoints the list of detected GPS points (may be empty)
 @param latitude the latitude of the current location
 @param longitude the longitude of the current location
 */
-(void)nearbyGPSController:(VDARNearbyGPSController*)controller receivedNewNearbyGPSPoints:(NSArray<VDARGPSPoint *> *) gpsPoints forLatitude:(double) latitude andLongitude:(double) longitude;

@end


@interface VDARNearbyGPSController : NSObject

#if __has_feature(objc_arc)
@property (nonatomic,weak) id<VDARNearbyGPSControllerDelegate> delegate;
#else
@property (nonatomic,assign) id<VDARNearbyGPSControllerDelegate> delegate;
#endif // __has_feature(objc_arc)

/**
 When TRUE, the controller post a local notification for the detected nearby GPS points when the application is in background.
 */
@property (nonatomic) BOOL enableNotifications;

/**
 Current instance of the controller
 */
+ (VDARNearbyGPSController*) sharedInstance;

/**
 Starts the detection of new nearby GPS points with default parameters
 */
-(void)startNearbyGPSDetection;

/**
 Starts the detection of new nearby GPS points
 @param minDistance the minimum distance difference between between two location updates
 @param detectionInterval the number of seconds between 2 detections (i.e. after a detection, we must wait `detectionInterval` before receving again the GPS point). A value of 0 means infinite (i.e. a GPS point is detected only once)
 @param maxRadius the upper limit for the detection radius in meters. A value of 0 or below means no limit.
 */
-(void)startNearbyGPSDetectionWithMinDistance:(double) minDistance detectionInterval:(long) detectionInterval maxDetectionRadius:(float) maxRadius;

/**
 Stops the detection of nearby GPS points. It does not clear the list of detected GPS points.
 For that, call VDARSDKController#resetDetectedGPSPoints
 */
-(void)stopNearbyGPSDetection;

/**
 Call this from your AppDelegate for enabling the click on the local notifications.
 A click on a notification will open the associated context.
 */
-(void)enableGPSNotifications;

@end
