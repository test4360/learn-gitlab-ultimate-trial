//
// VDARVisionSensor.h
// PixLive SDK
// Version 7.3.0-9064fd9
//
// Built on 2021-06-29 at 21:00:40
//
// https://www.vidinoti.com
//
// Copyright 2011-2015 Vidinoti SA. All rights reserved.
//
// Any distribution of this file or any part of the PixLive SDK without
// Vidinoti SA express consentment is forbidden.
//

#import <VDARSDK/VDARSDK.h>

/**
 Represent a vision sensor that have a trigger state corresponding to wether or not the associated image has been recognized
 
 @since 5.2.0
 */
@interface VDARVisionSensor : VDARSensor

@end
